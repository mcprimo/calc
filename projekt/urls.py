from django.conf.urls import patterns, include, url
from django.contrib import admin
import settings
admin.autodiscover()

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'projekt.views.home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
    {'document_root': settings.STATIC_ROOT, 'show_indexes': True}),
    #url(r'^details/(?P<wpis_id>\d+)/$', 'projekt.views.details'),
    #url(r'^komentuj/(?P<wpis_id>\d+)/$', 'projekt.views.komentuj'),
    url(r'^licz/', 'projekt.views.licz'),
    url(r'^wykonaj/(?P<id>[\w\-]+)/$', 'projekt.views.wykonaj'),
    url(r'^wykonaj_in/', 'projekt.views.wykonaj_in'),
    url(r'^dostepne/(?P<id>[\w\-]+)/$', 'projekt.views.dostepne'),
    url(r'^dostepne_in/', 'projekt.views.dostepne_in'),
    url(r'^js.js', 'projekt.views.js'),
    url(r'^rejestruj/', 'projekt.views.rejestruj'),

    # url(r'^projekt/', include('projekt.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
