from celery.decorators import task
import requests
import json
from lxml import objectify
import couchdb
import random

@task()
def rejestracja():
    couch = couchdb.Server('http://194.29.175.241:5984')
    db = couch["calc"]

    for i in db:
        try:
            doc=db[i]
            if doc["host"]=="http://mcprimotest.cloudapp.net/":
                doc["active"]="true"
                db[i]=doc
                return "suc"
        except:
            pass

    db.save({"host":"http://mcprimotest.cloudapp.net/","active":"true","operator":"+","calculation":"licz/"})
    return "suc"


@task()
def operacje(op):
    couch = couchdb.Server('http://194.29.175.241:5984')
    db = couch["calc"]
    lista=[]
    lista.append(op)

    for i in db.view("utils/list_active"):
        operator=i.key
        if not (operator in lista):
            lista.append(operator)

    text="Dostepne opreacje to "

    for i in lista:
        text=text+i+" "

    return text


@task()
def policz(ilosc,op,a,b,view):
    couch = couchdb.Server('http://194.29.175.241:5984')
    db = couch["calc"]
    los=random.randint(0,ilosc)


    iter=-1
    for i in view:
        iter=iter+1
        if iter>=los:
            try:
                doc=db[i.id]
                if(doc["operator"]==op):
                    url=doc["host"]+doc["calculation"]
                    data={"number1":a,"number2":b}
                    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
                    r=requests.post(url,json.dumps(data),headers=headers)
                    odp=r.json()
                    try:
                        if bool(odp["success"]):
                            return odp["result"]
                    except:
                        return float(odp)
            except:
                pass

    for i in view:
            try:
                doc=db[i.id]
                if(doc["operator"]==op):
                    url=doc["host"]+doc["calculation"]
                    data={"number1":a,"number2":b}
                    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
                    r=requests.post(url,json.dumps(data),headers=headers)
                    odp=r.json()
                    try:
                        if bool(odp["success"]):
                            return odp["result"]
                    except:
                        return float(odp)
            except:
                pass

    return "blad"


@task()
def oblicz(tekst):
    couch = couchdb.Server('http://194.29.175.241:5984')
    db = couch["calc"]
    view=db.view("utils/list_active")

    ilosc=0
    for i in view:
        ilosc=ilosc+1

    lista=tekst.split(" ")
    operatory=operacje("+")
    stos=[]

    for i in lista:
        try:
            stos.append(float(i))
        except:
            try:
                a=stos.pop()
                b=stos.pop()
            except:
                return "Blad skladniowy"
            if (i=="+"):
                c=a+b
                stos.append(c)
            elif i in operatory:
                c=policz(ilosc,i,a,b,view)
                if c=="blad":
                    return "Blad wszystkich serwerow posredniczacych dla danego operatora"
                stos.append(c)
            else:
                return "Zastosowano niestniejacy operaor!"


    return stos[0]
